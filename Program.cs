﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonEXAMEN
{
    class Program
    {
        static void Main(string[] args)
        {
            //Primero obtenemos nuestra primera instacia, aqui se se esta creando por primera vez.

             Clasesingleton Primera = Clasesingleton.ObtenInstancia();

            //Podemos poner algo en nuestra primera instancia
            Primera.Datos("Jostin bajaña");
            Primera.Proceso();
            Console.WriteLine(Primera);
            Console.WriteLine("///////");


            //Así mismo como obtubimos la primera instancia, vamos a por la segunda instancia.

            Clasesingleton segundainst = Clasesingleton.ObtenInstancia();
            //Entonces comprobamos si que sea la misma instancia,
            //lo cual si lo es tendra el mismo estado de la instancia
            Console.WriteLine(segundainst);
            Console.ReadKey();
        }

    }
}
