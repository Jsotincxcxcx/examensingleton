﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Este programa demuestra el funcionamiento del patron de diseño singleton.

namespace SingletonEXAMEN
{
    
    class Clasesingleton
    {
        // En esta parte se guarda la instancia.
        private static Clasesingleton LaInstancia;

        //Datos propios de la clase
        private string ElNombre;
        //Este es un constructor que es privado
        private Clasesingleton()
        {
            ElNombre = "";
        }

        public static Clasesingleton ObtenInstancia()
        {
            //ponemos un if para verificar si no existe la instancia.
            if (LaInstancia == null)
            {
                //Cuando no existe pues instanciamos.
                LaInstancia = new Clasesingleton();
                // Ponemos un mensaje de que la intancia fue creada
                Console.WriteLine("La instancia fue creada");
            }

            //En esta parte usamos el retunr para retornar la instancia 
            return LaInstancia;

        }

        //En esta parte creamos un metodos que es propio de nuestra clase.
        public void Datos(String Nombre)
        {
            ElNombre = Nombre;
        }
        public void Proceso()
        {
            Console.WriteLine(" La persona " + ElNombre + " esta en linea ");
        }

    }
}